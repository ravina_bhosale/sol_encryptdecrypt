﻿using RNCryptor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptDecryptText
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Ravina";

            string password = "ravina1234";

            //encrypt the given string (PlainText to Encrypt)
            Encryptor encryptorObj = new Encryptor();
            var Encryptresult = encryptorObj.Encrypt(str,password);

            System.Console.WriteLine("Encrypted value = " + Encryptresult);
            System.Console.Read();

            //decrypt the encrypted value (Encrypt to Decrypt)
            Decryptor decryptorObj = new Decryptor();
            var Decryptresult = decryptorObj.Decrypt(Encryptresult,password);
            System.Console.WriteLine("Decrypted value= " + Decryptresult);
            System.Console.Read();
        }
    }
}
